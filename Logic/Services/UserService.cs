﻿using Logic.Dto;
using Microsoft.AspNetCore.Identity;
using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class UserService : IUserService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public UserService(UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<UserResponse> SignUpAsync(SignUpRequest request)
        {
            var user = new User { Email = request.Email, UserName = request.Email, Name = request.Name };
            var result = await _userManager.CreateAsync(user, request.Password);

            return !result.Succeeded ? null : new UserResponse { Name = request.Name, Email = request.Email, Id = Guid.Parse(user.Id) };
        }
    }
}
