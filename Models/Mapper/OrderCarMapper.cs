﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Interfaces;
using Models.Models;
using System;

namespace Models.Mapper
{
    public class OrderCarMapper : IEntityMappingConfiguration<OrderCar>
    {
        /// <summary>
        /// Order car entity type configuration
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<OrderCar> builder)
        {
            builder.ToTable("OrderCars").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasOne(x => x.Order).WithMany(x => x.OrderCars).HasForeignKey(x => x.OrderId);

            builder.HasData(
                new OrderCar
                {
                    Id = Guid.NewGuid(),
                    OrderId = Guid.Parse("69CC1B18-74C0-4E52-9645-7ED75F96D1F1"),
                    DueDate = DateTime.UtcNow,
                    Comments = "Car with half fuel tank",
                    CarId = Guid.Parse("1CD10F6A-1EEC-4359-9677-65DAD5E31DFB")
                },
                new OrderCar
                {
                    Id = Guid.NewGuid(),
                    OrderId = Guid.Parse("69CC1B18-74C0-4E52-9645-7ED75F96D1F1"),
                    DueDate = DateTime.UtcNow,
                    Comments = "Car with full fuel tank",
                    CarId = Guid.Parse("1C581EC6-4180-4799-8959-AD069D0ACBF4")
                },
                new OrderCar
                {
                    Id = Guid.NewGuid(),
                    OrderId = Guid.Parse("88A6EF8A-D208-42A1-AC0C-B741DD15A715"),
                    DueDate = DateTime.UtcNow,
                    Comments = "Car with half fuel tank",
                    CarId = Guid.Parse("1CD10F6A-1EEC-4359-9677-65DAD5E31DFB")
                },
                new OrderCar
                {
                    Id = Guid.NewGuid(),
                    OrderId = Guid.Parse("C9398BCD-72CE-4292-BCBC-5D2909A402AD"),
                    DueDate = DateTime.UtcNow,
                    Comments = "Car with full fuel tank",
                    CarId = Guid.Parse("1C581EC6-4180-4799-8959-AD069D0ACBF4")
                });
        }
    }
}
