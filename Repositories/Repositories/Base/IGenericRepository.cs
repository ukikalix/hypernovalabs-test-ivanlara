﻿using Models.Generic;

namespace Repositories.Repositories.Base
{
    public interface IGenericRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class, IModel
    {
    }
}
