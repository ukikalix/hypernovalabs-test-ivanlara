﻿using Logic.Dto;
using Logic.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace hypernova_test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    var claim = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email);
        //    if (claim == null)
        //        return Unauthorized();

        //    var token = await _userService.GetUserAsync(new GetUserRequest { Email = claim.Value });

        //    return Ok(token);
        //}

        //[Route("[action]")]
        //public async Task<IActionResult> SignIn(SignInRequest request)
        //{
        //    var token = await _userService.SignInAsync(request);

        //    return Ok(new { token.Token });
        //}

        //[HttpPost]
        //public async Task<IActionResult> SignUp(SignUpRequest request)
        //{
        //    var user = await _userService.SignUpAsync(request);
        //    if (user == null)
        //        return BadRequest();

        //    if (request.SignIn)
        //        return Ok(await GetToken(new SignInRequest { Email = request.Email, Password = request.Password }));

        //    return CreatedAtAction(nameof(Get), new { }, null);
        //}

        //private async Task<TokenResponse> GetToken(SignInRequest request)
        //    => await _userService.SignInAsync(request);
    }
}
