﻿using Models.Generic;
using System;

namespace Models
{
    public class UserCar : IModel
    {
        /// <summary>
        /// Id of the Product
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Id of Category
        /// </summary>
        public Guid CarId { get; set; }

        /// <summary>
        /// Navigation property Product
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Navigation property Category
        /// </summary>
        public virtual Car Car { get; set; }
    }
}
