﻿using Logic.Services.Base;
using Microsoft.AspNetCore.Mvc;
using Models.Generic;

namespace hypernova_test.Controllers.Base
{
    [ApiController]
    public abstract class ApiBase<T, TKey> : ControllerBase
        where T : class, IModel
    {
        private static IService<T, TKey> _service;

        public ApiBase(IService<T, TKey> service)
        {
            _service = service;
        }

    }
}
