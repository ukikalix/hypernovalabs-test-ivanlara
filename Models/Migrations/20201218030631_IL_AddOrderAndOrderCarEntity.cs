﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class IL_AddOrderAndOrderCarEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderCars",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: false),
                    CarId = table.Column<Guid>(nullable: false),
                    Comments = table.Column<string>(nullable: true),
                    DueDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderCars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderCars_Cars_CarId",
                        column: x => x.CarId,
                        principalTable: "Cars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderCars_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"),
                column: "CardRegistration",
                value: new Guid("6d478fb6-9e9c-4792-ab9f-33507a0f3d24"));

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"),
                column: "CardRegistration",
                value: new Guid("10e5c118-15b2-4e91-884b-e72d5b0e902a"));

            migrationBuilder.CreateIndex(
                name: "IX_OrderCars_CarId",
                table: "OrderCars",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderCars_OrderId",
                table: "OrderCars",
                column: "OrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderCars");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"),
                column: "CardRegistration",
                value: new Guid("ec91b7e5-0c37-4597-be48-7e767c94a21c"));

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"),
                column: "CardRegistration",
                value: new Guid("69ce37d0-9a21-44c3-8e01-6d11c783a89d"));
        }
    }
}
