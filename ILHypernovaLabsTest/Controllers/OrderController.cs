﻿using hypernova_test.Controllers.Base;
using Logic.Services.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace hypernova_test.Controllers
{
    public class OrderController : GenericActionController<Order, Guid>
    {
        private IGenericService<Order, Guid> _service;

        public OrderController(IGenericService<Order, Guid> service) : base(service)
            => _service = service;

        /// <summary>
        /// Get all orders with nested order cars
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
            => Ok(await _service.FindBy(o => o.OrderCars.Any()).Include(c => c.OrderCars)
                .Select(o => new Order
                {
                    Id = o.Id,
                    UserId = o.UserId,
                    Name = o.Name,
                    OrderCars = o.OrderCars.Select(c => new OrderCar
                    {
                        Id = c.Id,
                        Car = c.Car,
                        Comments = c.Comments,
                        DueDate = c.DueDate
                    }).ToList()
                }).ToListAsync());
    }
}
