## Ejecutar el proyecto

Este proyecto fue desarrollado en ASP.NET Core 3.1, para probar cada endpoint, debera ejecutarse primeramente
en la consola de Visual Studio el comando **update-database** y asi crear la base de datos con EF Core.

## Uso

Ambos features, se ejecutan utilizando un servicio generico para GET, PUT, POST, y DELETE, solamente existe un
endpoint que implementa un custom method para obtener los datos con relacion anidada.

Se ha mockeado algunos rows para poder ejecutar los endpoints GET para visualizar resultados.

### Endpoint para obtener, crear, modificar y eliminar Carros
_https://localhost:000/api/car_

### Endpoint para obtener, crear, modificar y eliminar Ordenes
_https://localhost:000/api/order_

### Endpoint para obtener Ordenes con Carros incluidos
_https://localhost:000/api/order/getAll