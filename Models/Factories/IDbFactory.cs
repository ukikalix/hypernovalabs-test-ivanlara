﻿using System.Data;
using System.Threading.Tasks;

namespace Models.Factories
{
    public interface IDbFactory
    {
        Task<IDbConnection> CreateConnectionAsync();
    }
}
