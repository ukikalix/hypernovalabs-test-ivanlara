﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Dto
{
	public class TokenResponse
	{
		public string Token { get; set; }
	}

	public class UserResponse
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Email { get; set; }
	}
}
