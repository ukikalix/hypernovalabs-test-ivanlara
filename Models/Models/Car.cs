﻿using Models.Models;
using System;
using System.Collections.Generic;

namespace Models
{
    public class Car : Catalog<Guid>
    {
        public Guid CardRegistration { get; set; }
        public CartType CartType { get; set; }
        public virtual ICollection<UserCar> Cars { get; set; }
    }
}
