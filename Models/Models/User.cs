﻿using Microsoft.AspNetCore.Identity;
using Models.Generic;
using System.Collections.Generic;

namespace Models
{
    public class User : IdentityUser, IModel
    {
        public string Name { get; set; }

        public virtual ICollection<UserCar> Cars { get; set; }
    }
}
