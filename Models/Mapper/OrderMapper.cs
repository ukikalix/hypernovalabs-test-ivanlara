﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Interfaces;
using Models.Models;
using System;
using System.Collections.Generic;

namespace Models.Mapper
{
    public class OrderMapper : IEntityMappingConfiguration<Order>
    {
        /// <summary>
        /// Order entity type configuration
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasData(
                new Order
                {
                    Id = Guid.Parse("69CC1B18-74C0-4E52-9645-7ED75F96D1F1"),
                    Name = "Jose Canseco",
                    UserId = "F2CE02EA-068B-471F-8F4D-B2941126E4A8"
                },
                new Order
                {
                    Id = Guid.Parse("88A6EF8A-D208-42A1-AC0C-B741DD15A715"),
                    Name = "Marien Mendoza",
                    UserId = "21C98D02-0071-4760-9CD1-92426A4ED4DD"
                },
                new Order
                {
                    Id = Guid.Parse("C9398BCD-72CE-4292-BCBC-5D2909A402AD"),
                    Name = "Pedro Infante",
                    UserId = "6597B934-412D-49DA-AA93-E1A7B2EF5551"
                });
        }
    }
}
