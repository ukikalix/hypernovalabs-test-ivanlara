﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Mapper
{
    public class UserCarMapper : IEntityMappingConfiguration<UserCar>
    {
        /// <summary>
        /// User car entity type configuration
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<UserCar> builder)
        {
            builder.ToTable("UserCars").HasKey(x => new { x.UserId, x.CarId});
            builder.HasOne(x => x.User).WithMany(x => x.Cars).HasForeignKey(x => x.UserId);
        }
    }
}
