﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Models
{
    public class Order : Catalog<Guid>
    {
        public string UserId { get; set; }
        public ICollection<OrderCar> OrderCars { get; set; }
    }
}
