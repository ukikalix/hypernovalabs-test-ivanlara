﻿namespace Models.Generic
{
    public interface IModel
    {
    }

    public interface IModel<T> : IModel
    {
        /// <summary>
        /// Id property
        /// </summary>
        T Id { get; set; }
    }
}
