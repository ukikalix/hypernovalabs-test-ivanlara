﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Models.Uow
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        private IServiceProvider serviceProvider;

        public TContext Context { get; }

        public UnitOfWork(TContext context, IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            Context = context;
        }

        public int Commit()
        => Context.SaveChanges();

        public Task<int> CommitAsync()
        => Context.SaveChangesAsync();

        public void RollBack()
        => Context.Database.RollbackTransaction();

        public IDbContextTransaction BeginTransaction()
        => Context.Database.BeginTransaction();

        public void CommitTransaction()
        => Context.Database.CommitTransaction();

        public IExecutionStrategy CreateStrategy()
        => Context.Database.CreateExecutionStrategy();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected bool _isDisposed;

        protected void CheckDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException("The UnitOfWork is already disposed and cannot be used anymore.");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (Context != null)
                    {
                        Context.Dispose();
                    }
                }
            }
            _isDisposed = true;
        }
    }
}
