﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Interfaces;
using System;

namespace Models.Mapper
{
    public class CarMapper : IEntityMappingConfiguration<Car>
    {
        /// <summary>
        /// Car entity type configuration
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder.ToTable("Cars").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasData(
                new Car
                {
                    Id = Guid.Parse("1CD10F6A-1EEC-4359-9677-65DAD5E31DFB"),
                    Name = "Eon",
                    CardRegistration = Guid.NewGuid(),
                    CartType = Models.CartType.Hatchback
                },
                new Car
                {
                    Id = Guid.Parse("A2A1B034-456F-42F7-8B7E-1C6B649660E6"),
                    Name = "Swift",
                    CardRegistration = Guid.NewGuid(),
                    CartType = Models.CartType.Sedan
                },
                new Car
                {
                    Id = Guid.Parse("1C581EC6-4180-4799-8959-AD069D0ACBF4"),
                    Name = "Elantra",
                    CardRegistration = Guid.NewGuid(),
                    CartType = Models.CartType.Sedan
                });
        }
    }
}
