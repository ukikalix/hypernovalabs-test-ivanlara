﻿using Models.Generic;
using System;

namespace Models.Models
{
    public class OrderCar : IModel<Guid>
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid CarId { get; set; }
        public virtual Order Order { get; set; }
        public virtual Car Car { get; set; }
        public string Comments { get; set; }
        public DateTime DueDate { get; set; }
    }
}
