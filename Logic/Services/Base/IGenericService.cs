﻿using Models.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Services.Base
{
    public interface IGenericService<TEntity, TKey> : IService<TEntity, TKey>
        where TEntity : class, IModel
    {
    }
}
