﻿using hypernova_test.Controllers.Base;
using Logic.Services.Base;
using Models;
using System;

namespace hypernova_test.Controllers
{
    public class CarController : GenericActionController<Car, Guid>
    {
        private IGenericService<Car, Guid> _service;

        public CarController(IGenericService<Car, Guid> service) : base(service)
            => _service = service;
    }
}
