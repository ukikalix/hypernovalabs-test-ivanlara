﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositories.Repositories.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity entity);
        void Delete(TEntity entity);
        Task<bool> Any(Expression<Func<TEntity, bool>> predicate);
        void Edit(TEntity entity);
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindByKey(params object[] keyValues);
        IQueryable<TEntity> GetAll();
        DbSet<TEntity> GetDbSet();
        Task Save();
    }
}
