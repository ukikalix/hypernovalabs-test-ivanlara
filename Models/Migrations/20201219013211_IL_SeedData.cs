﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class IL_SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"),
                column: "CardRegistration",
                value: new Guid("04ae50bb-980d-49d2-8019-316766b342a5"));

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"),
                column: "CardRegistration",
                value: new Guid("ca98cc0b-5616-45c1-9448-85f556a06200"));

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "CardRegistration", "CartType", "Name" },
                values: new object[] { new Guid("a2a1b034-456f-42f7-8b7e-1c6b649660e6"), new Guid("39e365b5-c504-46e0-b877-e186014c4546"), (byte)0, "Swift" });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "Name", "UserId" },
                values: new object[,]
                {
                    { new Guid("69cc1b18-74c0-4e52-9645-7ed75f96d1f1"), "Jose Canseco", "F2CE02EA-068B-471F-8F4D-B2941126E4A8" },
                    { new Guid("88a6ef8a-d208-42a1-ac0c-b741dd15a715"), "Marien Mendoza", "21C98D02-0071-4760-9CD1-92426A4ED4DD" },
                    { new Guid("c9398bcd-72ce-4292-bcbc-5d2909a402ad"), "Pedro Infante", "6597B934-412D-49DA-AA93-E1A7B2EF5551" }
                });

            migrationBuilder.InsertData(
                table: "OrderCars",
                columns: new[] { "Id", "CarId", "Comments", "DueDate", "OrderId" },
                values: new object[,]
                {
                    { new Guid("cc21b2db-88f7-406c-9536-0211f96a2fac"), new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"), "Car with half fuel tank", new DateTime(2020, 12, 19, 1, 32, 10, 907, DateTimeKind.Utc).AddTicks(2512), new Guid("69cc1b18-74c0-4e52-9645-7ed75f96d1f1") },
                    { new Guid("4005baf3-84f7-4b7c-afcd-f6bb78b8dd90"), new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"), "Car with full fuel tank", new DateTime(2020, 12, 19, 1, 32, 10, 907, DateTimeKind.Utc).AddTicks(3724), new Guid("69cc1b18-74c0-4e52-9645-7ed75f96d1f1") },
                    { new Guid("142b5a40-fe1f-44b5-880b-a50c39383d6e"), new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"), "Car with half fuel tank", new DateTime(2020, 12, 19, 1, 32, 10, 907, DateTimeKind.Utc).AddTicks(3764), new Guid("88a6ef8a-d208-42a1-ac0c-b741dd15a715") },
                    { new Guid("a4240188-f5a5-4260-ae90-b97b6975b760"), new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"), "Car with full fuel tank", new DateTime(2020, 12, 19, 1, 32, 10, 907, DateTimeKind.Utc).AddTicks(3768), new Guid("c9398bcd-72ce-4292-bcbc-5d2909a402ad") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("a2a1b034-456f-42f7-8b7e-1c6b649660e6"));

            migrationBuilder.DeleteData(
                table: "OrderCars",
                keyColumn: "Id",
                keyValue: new Guid("142b5a40-fe1f-44b5-880b-a50c39383d6e"));

            migrationBuilder.DeleteData(
                table: "OrderCars",
                keyColumn: "Id",
                keyValue: new Guid("4005baf3-84f7-4b7c-afcd-f6bb78b8dd90"));

            migrationBuilder.DeleteData(
                table: "OrderCars",
                keyColumn: "Id",
                keyValue: new Guid("a4240188-f5a5-4260-ae90-b97b6975b760"));

            migrationBuilder.DeleteData(
                table: "OrderCars",
                keyColumn: "Id",
                keyValue: new Guid("cc21b2db-88f7-406c-9536-0211f96a2fac"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("69cc1b18-74c0-4e52-9645-7ed75f96d1f1"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("88a6ef8a-d208-42a1-ac0c-b741dd15a715"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("c9398bcd-72ce-4292-bcbc-5d2909a402ad"));

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1c581ec6-4180-4799-8959-ad069d0acbf4"),
                column: "CardRegistration",
                value: new Guid("6d478fb6-9e9c-4792-ab9f-33507a0f3d24"));

            migrationBuilder.UpdateData(
                table: "Cars",
                keyColumn: "Id",
                keyValue: new Guid("1cd10f6a-1eec-4359-9677-65dad5e31dfb"),
                column: "CardRegistration",
                value: new Guid("10e5c118-15b2-4e91-884b-e72d5b0e902a"));
        }
    }
}
