﻿using Logic.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public interface IUserService
    {
        Task<UserResponse> SignUpAsync(SignUpRequest request);
    }
}
