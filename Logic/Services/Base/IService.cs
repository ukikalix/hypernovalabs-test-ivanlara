﻿using Models.Generic;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Logic.Services.Base
{
    public interface IService<TEntity, TKey> where TEntity : class, IModel
    {
        Task<TEntity> Create(TEntity entity);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> Find(params object[] keyValues);
        Task Modify(TKey id, TEntity entity);
        Task<TEntity> Delete(TKey id);
    }
}
